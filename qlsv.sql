create database QLSV;

use QLSV;

create table DMKHOA( MaKH varchar(6) primary key, 
                    TenKhoa varchar(30));

create table SINHVIEN( MaSV varchar(6) primary key, 
                    HoSV varchar(30), 
                    TenSV varchar(15),
                    GioiTinh char(1), 
                    NgaySinh datetime, 
                    NoiSinh varchar(50), 
                    DiaChi varchar(50), 
                    MaKH varchar(6) FOREIGN KEY REFERENCES DMKHOA(MaKH), 
                    HocBong int);

ALTER TABLE SINHVIEN ADD FOREIGN KEY(MaKH) REFERENCES DMKHOA(MaKH);

INSERT INTO DMKHOA(MaKH, TenKhoa) VALUES('CNTT', 'Công nghệ thông tin'),
                                        ('TOAN', 'Toán');

INSERT INTO SINHVIEN  VALUES('1', 'Cong', 'Dung', 'N', '2001-12-15 07:00:00', 'Thai Nguyen', 'Ha Noi', 'CNTT', 10), 
                            ('2', 'Tran', 'Huy', 'N', '2001-12-15 07:00:00', 'Ha Noi', 'Ha Noi', 'TOAN', 10),
                            ('3', 'Nguyen', 'Hai Anh', 'N', '2001-12-15 07:00:00', 'Ha Noi', 'Ha Noi', 'CNTT', 10);